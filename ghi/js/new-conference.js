window.addEventListener('DOMContentLoaded', async () => {
// looking for an event to occur on HTML, async is saying that
// it does not need to occur at the same time as the
    const url = 'http://localhost:8000/api/locations/';
//  declaring variable for where we are pulling data that is stored in the API
    const response = await fetch(url);
//  declaring variable for the gathering the data from the API
    if (response.ok) {// if the action of gathering the data is okay:
        const data = await response.json();
// declaring variable for the data

        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option)
        }
      }
    });
    const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }

    });
