function createCard(title, description, pictureUrl, startDate, endDate, location) {
    return `
    <div class="shadow-lg p-3 mb-5 bg-white rounded"</div>
      <div class="card" >
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-muted">${startDate} - ${endDate}</small>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {


    const url = 'http://localhost:8000/api/conferences/';


    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log('not ok')
      } else {
        const data = await response.json();

        let i = 0
        for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log("details", details)
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString()
          const endDate = new Date(details.conference.ends).toLocaleDateString()
          const location = details.conference.location.name
          const html = createCard(title, description, pictureUrl, startDate, endDate, location);
          const column = document.querySelector(`#col-${i % 3}`);
          column.innerHTML +=html;
          i += 1

        }
      }
      }

    } catch (e) {
      console.error('error', error);
    }

  });
